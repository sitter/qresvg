<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
-->

# Better SVG for Qt

```bash
LD_PRELOAD=`pwd`/bin/libqresvg.so systemsettings
```

This is an interposed library for QSvgRenderer backing it with resvg rather than the limited internal QtSVG
implementation.

Based on the c bindings of https://github.com/RazrFalcon/resvg
