// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

#include "qsvgrenderer.h"

#include <QFont>
#include <QGuiApplication>
#include <QPainter>
#include <private/qobject_p.h>

#include "ResvgQt.h"

namespace
{
constexpr auto DEFAULT_DPI = 96;
} // namespace

class Q_DECL_HIDDEN QSvgRendererPrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(QSvgRenderer)
public:
    ResvgOptions options() const
    {
        ResvgOptions options;
        options.setFontFamily(font.family());
        options.setFontSize(font.pointSizeF());
        options.setDpi(DEFAULT_DPI * dpr);
        return options;
    }

    void applyPainter(QPainter *painter)
    {
        if (painter->font() != font || (painter->device() && painter->device()->devicePixelRatioF() != dpr)) {
            font = painter->font();
            if (painter->device()) {
                dpr = painter->device()->devicePixelRatioF();
            }
            std::visit([this](const auto &arg) { renderer.load(arg, options()); }, data);
        }
    }

    ResvgRenderer renderer;
    QFont font = qGuiApp->font();
    qreal dpr = qGuiApp->devicePixelRatio();

    std::variant<QString, QByteArray> data;
};

QSvgRenderer::QSvgRenderer(QObject *parent)
    : QObject(*new QSvgRendererPrivate, parent)
{
}

QSvgRenderer::QSvgRenderer(const QString &filename, QObject *parent)
    : QSvgRenderer(parent)
{
    load(filename);
}

QSvgRenderer::QSvgRenderer(const QByteArray &contents, QObject *parent)
    : QSvgRenderer(parent)
{
    load(contents);
}

QSvgRenderer::QSvgRenderer(QXmlStreamReader *contents, QObject *parent)
    : QSvgRenderer(parent)
{
    load(contents);
}

QSvgRenderer::~QSvgRenderer() = default;

bool QSvgRenderer::isValid() const
{
    Q_D(const QSvgRenderer);
    return d->renderer.isValid();
}

QSize QSvgRenderer::defaultSize() const
{
    Q_D(const QSvgRenderer);
    return d->renderer.defaultSize();
}

QRect QSvgRenderer::viewBox() const
{
    Q_D(const QSvgRenderer);
    return d->renderer.viewBox();
}

QRectF QSvgRenderer::viewBoxF() const
{
    Q_D(const QSvgRenderer);
    return d->renderer.viewBoxF();
}

void QSvgRenderer::setViewBox(const QRect &viewbox)
{
    qWarning() << Q_FUNC_INFO << "not supported";
}

void QSvgRenderer::setViewBox(const QRectF &viewbox)
{
    qWarning() << Q_FUNC_INFO << "not supported";
}

Qt::AspectRatioMode QSvgRenderer::aspectRatioMode() const
{
    qWarning() << Q_FUNC_INFO << "not supported";
    return Qt::IgnoreAspectRatio;
}

void QSvgRenderer::setAspectRatioMode(Qt::AspectRatioMode mode)
{
    qWarning() << Q_FUNC_INFO << "not supported";
}

int QSvgRenderer::animationDuration() const
{
    qWarning() << Q_FUNC_INFO << "not supported";
    return -1;
}

bool QSvgRenderer::animated() const
{
    qWarning() << Q_FUNC_INFO << "not supported";
    return false;
}

int QSvgRenderer::framesPerSecond() const
{
    qWarning() << Q_FUNC_INFO << "not supported";
    return -1;
}

void QSvgRenderer::setFramesPerSecond(int num)
{
    qWarning() << Q_FUNC_INFO << "not supported";
}

int QSvgRenderer::currentFrame() const
{
    qWarning() << Q_FUNC_INFO << "not supported";
    return -1;
}

void QSvgRenderer::setCurrentFrame(int)
{
    qWarning() << Q_FUNC_INFO << "not supported";
}

QRectF QSvgRenderer::boundsOnElement(const QString &id) const
{
    Q_D(const QSvgRenderer);
    return d->renderer.boundsOnElement(id);
}

bool QSvgRenderer::elementExists(const QString &id) const
{
    Q_D(const QSvgRenderer);
    return d->renderer.elementExists(id);
}

QTransform QSvgRenderer::transformForElement(const QString &id) const
{
    Q_D(const QSvgRenderer);
    return d->renderer.transformForElement(id);
}

bool QSvgRenderer::load(const QString &filename)
{
    Q_D(QSvgRenderer);
    d->data = filename;
    return d->renderer.load(filename, d->options());
}

bool QSvgRenderer::load(const QByteArray &contents)
{
    Q_D(QSvgRenderer);
    d->data = contents;
    return d->renderer.load(contents, d->options());
}

bool QSvgRenderer::load(QXmlStreamReader *contents)
{
    Q_D(QSvgRenderer);
    d->data = contents->device()->readAll();
    return d->renderer.load(std::get<QByteArray>(d->data), d->options());
}

void QSvgRenderer::render(QPainter *p)
{
    Q_D(QSvgRenderer);
    d->applyPainter(p);
    const auto image = d->renderer.renderToImage();
    QRectF rect(0, 0, image.width(), image.height());
    p->drawImage(rect, image);
}

void QSvgRenderer::render(QPainter *p, const QRectF &bounds)
{
    Q_D(QSvgRenderer);
    d->applyPainter(p);
    const auto image = d->renderer.renderToImage(bounds.size().toSize());
    p->drawImage(bounds, image);
}

void QSvgRenderer::render(QPainter *p, const QString &elementId, const QRectF &bounds)
{
    qWarning() << Q_FUNC_INFO << "not supported";
}
